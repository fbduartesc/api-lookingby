require('dotenv').config();
const express = require('express');
const cors = require('cors');
const { ApolloServer, gql } = require('apollo-server-express');
const { importSchema } = require('graphql-import');

const app = express();

app.use(cors());
app.use(express.json());

const resolvers = require('./resolvers');

const schemaPath = './schema/index.graphql';

const server = new ApolloServer({
    typeDefs: importSchema(schemaPath),
    resolvers,
    introspection: true,
    playground: true
});

server.applyMiddleware({
    app,
    path: '/'
});

app.listen({ port: process.env.PORT || 4001 }, () => {
    console.log('Servidor em execução')
});