const api = require('../../services/api');

module.exports = {
    async users(_, args, ctx) {
        return await api.get('users').then(response => response.data);
    },
    async user(_, {filtro}, ctx) {
        const {id} = filtro;
        return await api.get(`users/${id}`).then(response => response.data);
    },
    async userPosts(_, {filtro}, ctx) {
        const {id} = filtro;
        const user = await api.get(`users/${id}`).then(response => response.data);
        const posts = await api.get(`users/${id}/posts`).then(response => response.data);
        return {...user, posts};
    },
}