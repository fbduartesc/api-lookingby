const api = require('../../services/api');
module.exports = {    
    async company(_, args, ctx) {
        const response = await api.get('users').then(response => response.data);
        let companies = response.map(result => {
            result.company.id = result.id;
            return result.company;
        });
        return companies;
    },
}