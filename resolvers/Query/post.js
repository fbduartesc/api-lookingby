const api = require('../../services/api');

module.exports = {
    async posts(_, {filtro}, ctx) {
        const {name} = filtro;
        const users = await api.get('users').then(response => response.data);
        const posts = await api.get('posts').then(response => response.data);        
        return posts.map((post) => {
            post.user = users.filter((user, index, array) => {
                return user.id === post.userId;
            })[0];
            return post;
        });
    },
    async post(_, {filtro}, ctx) {
        const {id} = filtro;
        const post = await api.get(`posts/${id}`).then(response => response.data);
        const users = await api.get(`users/${post.userId}`).then(response => response.data);        
        return {...post, user:users}
    },
}