const post = require('./post');

it('deve retornar todos os posts', async () => {
    const req = await post.posts(null, {filtro: {name:'teste'}});
    expect(req.length).toBe(100);
});

it('deve retornar o post com id 1', async () => {
    const req = await post.post(null, {filtro: {id:1}});
    expect(typeof req).toBe('object');    
});

it('deve retornar o nome de usuario "Leanne Graham"', async () => {
    const req = await post.post(null, {filtro: {id:1}});
    expect(req.user.name).toBe('Leanne Graham');
});