const user = require('./user');

it('deve retornar 10 empresas', async () => {
    const req = await user.users();
    expect(req.length).toBe(10);
});

it('deve retornar apenas um usuário', async () => {
    const req = await user.user(null, {filtro: {id:1}});
    expect(req.name).toBe('Leanne Graham');
});

it('deve retornar todos os posts de apenas um usuário', async () => {
    const req = await user.userPosts(null, {filtro: {id:1}});
    expect(req.posts.length).toBe(10);
});