const company = require('./company');

it('deve retornar 10 empresas', async () => {
    const req = await company.company();
    expect(req.length).toBe(10);
});

it('deve retornar apenas uma empresa', async () => {
    const req = await company.company();
    expect(req[0].name).toBe('Romaguera-Crona');
});