const user = require('./user')
const company = require('./company')
const post = require('./post')

 module.exports = {
    ...user,
    ...company,
    ...post
 }